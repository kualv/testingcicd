FROM golang:1.22
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o /gotestfile
EXPOSE 8080
RUN chmod +x /gotestfile
CMD ["/gotestfile"]